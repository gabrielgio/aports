# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=bmake
pkgver=20230723
pkgrel=0
pkgdesc="Portable version of the NetBSD make build tool"
url="http://www.crufty.net/help/sjg/bmake.html"
arch="all"
license="BSD-2-Clause"
subpackages="$pkgname-doc"
checkdepends="tzdata"
source="http://www.crufty.net/ftp/pub/sjg/bmake-$pkgver.tar.gz
	separate-tests.patch"
builddir="$srcdir/$pkgname"

# Reset MAKEFLAGS since it might contain options not supported
# by bmake. This is, for instance, the case on the builders.
export MAKEFLAGS="-j${JOBS:-1}"

prepare() {
	default_prepare

	# FIXME: Disable tests failing on musl.
	sed -i unit-tests/Makefile \
		-e "/deptgt-delete_on_error/d"
}

build() {
	sh ./boot-strap --with-default-sys-path=/usr/share/mk op=build
}

check() {
	sh ./boot-strap op=test
}

package() {
	sh ./boot-strap --prefix=/usr --with-mksrc=/usr/share/mk \
			--install-destdir="$pkgdir" op=install

	rm -rf "$pkgdir"/usr/share/man/cat1
	install -Dm644 bmake.1 \
		"$pkgdir"/usr/share/man/man1/bmake.1

	mkdir -p "$pkgdir"/usr/share/doc/$pkgname/
	install -m644 README ChangeLog \
		"$pkgdir"/usr/share/doc/$pkgname/
}

sha512sums="
befd752ba83350751956c84bf08b59bb90dd1468a79d870d368e2989009f29152011aa81a1b69e112f3a6d033bd0d0643fe8b00d67d71c7f2bdf5f47a3a614f5  bmake-20230723.tar.gz
04217b04aca4252f54c836e982d95106a09166370f84fa672c418d1b1799adb9697f5ac9eb10a6ee3a8527e39196a37ad92bb5945733407bf9ec1a7f223183bb  separate-tests.patch
"
