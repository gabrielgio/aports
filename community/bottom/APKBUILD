# Contributor: guddaff <guddaff@protonmail.com>
# Maintainer: guddaff <guddaff@protonmail.com>
pkgname=bottom
pkgver=0.9.5
pkgrel=0
pkgdesc="Graphical process/system monitor with a customizable interface"
url="https://github.com/ClementTsang/bottom"
# s390x: fails to build nix crate
arch="all !s390x"
license="MIT"
makedepends="cargo cargo-auditable"
subpackages="
	$pkgname-fish-completion
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-doc
	"
source="https://github.com/ClementTsang/bottom/archive/$pkgver/bottom-$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	BTM_GENERATE=true cargo auditable build --frozen --release
}

check() {
	CARGO_HUSKY_DONT_INSTALL_HOOKS=true cargo test --frozen
}

package() {
	install -Dm755 target/release/btm -t "$pkgdir"/usr/bin/

	install -Dm644 sample_configs/default_config.toml -t "$pkgdir"/usr/share/doc/$pkgname/

	cd target/tmp/bottom/completion
	install -Dm644 _btm "$pkgdir"/usr/share/zsh/site-functions/_btm
	install -Dm644 btm.bash "$pkgdir"/usr/share/bash-completion/completions/btm
	install -Dm644 btm.fish "$pkgdir"/usr/share/fish/vendor_completions.d//btm.fish
}

sha512sums="
c3690118c3a8ea3293ab9a354e83485ef1f492d29a11e567b77b01de0e01b28527fd161499f9fb4d43253e3da8491f0732b90016182f0aac41ade9b8f500a527  bottom-0.9.5.tar.gz
"
