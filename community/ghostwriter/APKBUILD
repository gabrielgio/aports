# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=ghostwriter
pkgver=23.08.0
pkgrel=0
pkgdesc="Distraction-free markdown editor"
url="https://invent.kde.org/office/ghostwriter/"
arch="aarch64 x86_64"
license="GPL-3.0-or-later"
depends="qt5-qtsvg"
makedepends="
	extra-cmake-modules
	hunspell-dev
	sonnet-dev
	kcoreaddons-dev
	kdoctools-dev
	kxmlgui-dev
	kconfigwidgets-dev
	qt5-qtsvg-dev
	qt5-qttools-dev
	qt5-qtbase-dev
	qt5-qtwebchannel-dev
	qt5-qtwebengine-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/office/ghostwriter.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/ghostwriter-$pkgver.tar.xz"
options="!check" # no tests

build() {
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3e6463c704b16a2adccae99c3db866db8ddd56642bda2cf98400d184ce058d62ffe2eea284e3cff79128b69a65cf9267bae5b440be9e3f62a357653c8fac1eef  ghostwriter-23.08.0.tar.xz
"
