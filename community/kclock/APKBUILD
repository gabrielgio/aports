# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kclock
pkgver=23.08.0
pkgrel=0
pkgdesc="Clock app for Plasma Mobile"
url="https://invent.kde.org/utilities/kclock"
# armhf blocked by qt5-qtdeclarative
# x86 broken
arch="all !armhf !x86"
license="LicenseRef-KDE-Accepted-GPL"
depends="
	kirigami-addons
	kirigami2
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kirigami-addons-dev
	kirigami2-dev
	knotifications-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/kclock.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kclock-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9e1b6739c97efe1d9e287c2ffb8c668afd802c8ab2661d9ae7f882672fbf49b2e2acbc21e6dfb0613b7ecafcde97be6bb416dd1bc65b4ab9b18637f8efcb5e26  kclock-23.08.0.tar.xz
"
