# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kontrast
pkgver=23.08.0
pkgrel=0
pkgdesc="Tool to check contrast for colors that allows verifying that your colors are correctly accessible"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://invent.kde.org/accessibility/kontrast"
license="GPL-3.0-or-later AND CC0-1.0"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	futuresql-dev
	kcoreaddons-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	"
_repo_url="https://invent.kde.org/accessibility/kontrast.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kontrast-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d6de006beebce7a946c418e3f0d4d4b20208642646e9962b50451f9c3cf271ff58af86d04d2ff99e451e4e08cc49b6d9449d598115324e62af9e93d4ea7d3d18  kontrast-23.08.0.tar.xz
"
