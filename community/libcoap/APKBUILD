# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=libcoap
pkgver=4.3.2
pkgrel=0
pkgdesc="A CoAP (RFC 7252) implementation"
url="https://libcoap.net/"
arch="all"
license="BSD-2-Clause"
makedepends="automake autoconf libtool asciidoc openssl-dev>3 cunit-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/obgm/libcoap/archive/v$pkgver.tar.gz"

build() {
	./autogen.sh
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--with-openssl \
		--disable-doxygen \
		--enable-tests
	make
}

check() {
	./tests/testdriver
}

package() {
	make DESTDIR="$pkgdir" install

	mkdir -p "$pkgdir"/usr/share/doc/$pkgname
	install -m644 CONTRIBUTE AUTHORS ChangeLog NEWS README.md TODO \
		"$pkgdir"/usr/share/doc/$pkgname
}

sha512sums="
9c1aa1c0d7d183646eb69a860514d5cecb521e20f66ad5f9fafb77c574355f2be2d8156c989d432a23a47f6cdfef33ce167922df44ae610eb0a52454e780d1f5  libcoap-4.3.2.tar.gz
"
