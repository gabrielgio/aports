# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=lokalize
pkgver=23.08.0
pkgrel=0
pkgdesc="Computer-Aided Translation System"
url="https://apps.kde.org/lokalize/"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="(GPL-2.0-only OR GPL-3.0-only) AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	hunspell-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knotifications-dev
	kross-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	sonnet-dev
	"
_repo_url="https://invent.kde.org/sdk/lokalize.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/lokalize-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
195915a4e35b56a410567d06893054a1f34cfb441cfc966ac0cfb3c63d5e0df265b1c2d408af1c076e2101abea451a0f49054dad881055e3706ceb7dc9dbc3a8  lokalize-23.08.0.tar.xz
"
