# Contributor: Curt Tilmes <Curt.Tilmes@nasa.gov>
# Maintainer: Curt Tilmes <Curt.Tilmes@nasa.gov>
pkgname=nqp
pkgver=2023.08
pkgrel=0
pkgdesc="Not Quite Perl"
url="https://github.com/Raku/nqp"
# riscv64 blocked by moarvm
# ppc64le, s390x: fails check
arch="all !ppc64le !s390x !riscv64"
options="!archcheck" # Arch dependencies are embedded
license="Artistic-2.0"
depends="moarvm~$pkgver"
makedepends="perl-utils moarvm-dev~$pkgver"
checkdepends="perl-test-harness"
subpackages="$pkgname-doc"
source="https://github.com/Raku/nqp/releases/download/$pkgver/nqp-$pkgver.tar.gz"

build() {
	perl Configure.pl --prefix=/usr --backends=moar
	make -j"$JOBS"
}

check() {
	# Tests take too long to run on these architectures
	if [ "$CARCH" = "x86" ] || [ "$CARCH" = "armhf" ]; then
		./nqp -V
	else
		export TEST_JOBS=$JOBS
		export HARNESS_VERBOSE=1

		msg "Running $TEST_JOBS parallel test jobs"
		make test
	fi
}

package() {
	make DESTDIR="$pkgdir" install

	install -Dvm644 CREDITS LICENSE README.pod VERSION \
		-t "$pkgdir"/usr/share/doc/"$pkgname"
	cp -vr docs examples "$pkgdir"/usr/share/doc/"$pkgname"/
}

sha512sums="
3f4ef58f49fca0a35036e0e7bc4f251a96df651d8fadc3c9b187d322b3f2785474dea12142e7c5f57472ba44523345576dfbec235aee85a70b779853193386fb  nqp-2023.08.tar.gz
"
