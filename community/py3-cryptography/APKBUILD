# Contributor: August Klein <amatcoder@gmail.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-cryptography
pkgver=41.0.3
pkgrel=0
pkgdesc="Cryptographic recipes and primitives for Python"
url="https://cryptography.io/"
arch="all"
license="Apache-2.0 OR BSD-3-Clause"
depends="python3 py3-cffi"
makedepends="
	libffi-dev
	openssl-dev>3
	py3-gpep517
	py3-setuptools
	py3-setuptools-rust
	py3-wheel
	python3-dev
	"
checkdepends="
	py3-hypothesis
	py3-iso8601
	py3-pretend
	py3-pytest
	py3-pytest-benchmark
	py3-pytest-subtests
	py3-pytest-xdist
	py3-tz
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/c/cryptography/cryptography-$pkgver.tar.gz
	https://files.pythonhosted.org/packages/source/c/cryptography_vectors/cryptography_vectors-$pkgver.tar.gz
	"
builddir="$srcdir/cryptography-$pkgver"
options="net"

replaces="py-cryptography" # Backwards compatibility
provides="py-cryptography=$pkgver-r$pkgrel" # Backwards compatibility

# secfixes:
#   41.0.2-r0:
#     - CVE-2023-38325
#   39.0.1-r0:
#     - CVE-2023-23931
#   3.2.2-r0:
#     - CVE-2020-36242
#   3.2.1-r0:
#     - CVE-2020-25659

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2

	# prepare cryptography vectors for testing
	cd "$srcdir/cryptography_vectors-$pkgver"
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages test-env
	test-env/bin/python3 -m installer .dist/cryptography*.whl
	test-env/bin/python3 -m installer "$srcdir"/cryptography_vectors-$pkgver/.dist/cryptography*.whl
	test-env/bin/python3 -m pytest -n $JOBS
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/cryptography-*.whl
}

sha512sums="
84f79f72be3646c1e9a932ba8aba514471f6c0e50462a0bb3a927622ce67c7932e71d44e187db8928b05f04b803b958284f785c0dc297a0c9453846af1ac1aca  cryptography-41.0.3.tar.gz
1f794f2bf4942ec47c93178061fc581c8ab87725927e7e6f0c939e6c499c98867296f87b39583f30ab42e3e682c5be8ef0aba705adf7390898df52ec3a822621  cryptography_vectors-41.0.3.tar.gz
"
