# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=maui
pkgname=station
pkgver=2.2.1
pkgrel=0
pkgdesc="Convergent terminal emulator"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/maui/station"
license="GPL-3.0-or-later"
depends="qmltermwidget"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	ki18n-dev
	mauikit-dev
	mauikit-filebrowsing-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"
_repo_url="https://invent.kde.org/maui/station.git"
source="https://download.kde.org/stable/maui/station/$pkgver/station-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1612609956783b1015a0e0230d5e545035750de51f51d81968b984fc935c0153d2c21dcb546d16273ef9afafc56e4683a8882c2e83196b53aab36dfc2fb33edd  station-2.2.1.tar.xz
"
