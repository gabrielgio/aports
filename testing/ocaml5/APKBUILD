# Contributor: Borys Zhukov <mp5@mp5.im>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Contributor: omni <omni+alpine@hack.org>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=ocaml5
provides=ocaml
pkgver=5.0.0
pkgrel=1
pkgdesc="Main implementation of the Caml programming language"
url="https://ocaml.org/"
arch="all !riscv64" # follow community/ocaml aport
license="LGPL-2.1-or-later WITH OCaml-LGPL-linking-exception"
depends="$pkgname-runtime=$pkgver-r$pkgrel gcc"
checkdepends="parallel"
subpackages="
	$pkgname-doc
	$pkgname-ocamldoc
	$pkgname-compiler-libs:_compiler_libs
	$pkgname-runtime
	"
source="https://caml.inria.fr/pub/distrib/ocaml-${pkgver%.*}/ocaml-$pkgver.tar.xz"
builddir="$srcdir/ocaml-$pkgver"

build() {
	local _make _conf
	case "$CARCH" in
	# Current native compiler support in 5.0.x
	aarch64|x86_64)
		_make="world.opt"
		_conf="--enable-native-compiler \
			--enable-native-toplevel \
			--disable-installing-bytecode-programs"
		msg "Building native compiler for $CARCH"
		;;
	*)
		_make="world"
		_conf="--disable-native-compiler \
			--disable-native-toplevel \
			--enable-installing-bytecode-programs"
		msg "Building bytecode compiler for $CARCH"
		;;
	esac

	./configure \
		--prefix /usr \
		--bindir /usr/bin \
		--libdir /usr/lib/ocaml \
		--mandir /usr/share/man \
		--docdir /usr/share/doc/$pkgname \
		--disable-installing-source-artifacts \
		$_conf \
		CC="${CC:-gcc}" \
		AS="${CC:-gcc} -c" \
		ASPP="${CC:-gcc} -c"
	make $_make
}

check() {
	make ocamltest
	make -C testsuite parallel
}

package() {
	make DESTDIR="$pkgdir" install
}

ocamldoc() {
	pkgdesc="$pkgdesc (documentation generator)"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/bin/ocamldoc*
	amove usr/lib/ocaml/ocamldoc
}

_compiler_libs() {
	pkgdesc="$pkgdesc (compiler libraries)"
	depends="$pkgname=$pkgver-r$pkgrel"
	provides="$pkgname-dev=$pkgver-r$pkgrel"

	amove usr/lib/ocaml/compiler-libs
}

runtime() {
	pkgdesc="$pkgdesc (runtime environment)"
	depends=

	amove usr/bin/ocamlrun*

	local i; for i in \
		'*.cmo' \
		'*.cmi' \
		'*.cma' \
		'stublibs' \
		'threads/*.cmi' \
		'threads/*.cma'
	do
		amove "usr/lib/ocaml/$i"
	done
}

sha512sums="
1ddc5ae1cbdccdb44dd1bb9878470bbac3ba225d4339aae35220cac99dda2640c74d48e536111ee47e7fe2a9848db8581966a6f1e182bb102ffade0454dc4ecd  ocaml-5.0.0.tar.xz
"
