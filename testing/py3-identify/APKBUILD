# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=py3-identify
_pyname=identify
pkgver=2.5.27
pkgrel=0
pkgdesc="File identification library for Python"
url="https://github.com/pre-commit/identify"
arch="noarch"
license="MIT"
depends="py3-ukkonen"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="$_pyname-$pkgver.tar.gz::https://github.com/pre-commit/identify/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
a604088628b2aeeaf8cb04a91f197bbc567d3c3e17047400201cccae5b168d92ebac26e181b835fa7c18f78d051795d7c25165596c2636f882098f63bac527d6  identify-2.5.27.tar.gz
"
